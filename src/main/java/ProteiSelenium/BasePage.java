// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-07 16:53:58 roukoru>

package ProteiSelenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    public final int TTW = 16;
    public WebDriver webDriver;
    public WebDriverWait drvWait;

    BasePage (WebDriver driver) {
        webDriver = driver;
        drvWait = new WebDriverWait (webDriver, TTW);
    }

    public void waitVisibility (By element) {
        drvWait.until (ExpectedConditions.visibilityOfAllElementsLocatedBy (element));
    }
 
    public void click (By element) {
        waitVisibility (element);
        webDriver.findElement (element).click();
    }
 
    public void writeText (By element, String text) {
        waitVisibility (element);
        webDriver.findElement (element).sendKeys(text);
    }
 
    public String readText (By element) {
        waitVisibility (element);
        return webDriver.findElement (element).getText();
    }
}
