// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-10 08:55:09 roukoru>

package ProteiSelenium;

public class Main
{
    public static void main (String[] args) throws Exception {
        // java -jar target/Protei-1.0-SNAPSHOT-jar-with-dependencies.jar login.txt profile.txt
        LoginTest.main (args);
        ProfileTest.main (args);
    }

}
