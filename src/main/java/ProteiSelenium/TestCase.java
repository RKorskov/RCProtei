// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-10 08:58:33 roukoru>

package ProteiSelenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.nio.file.FileSystems;
import java.io.IOException;
import java.util.logging.Logger;

public class TestCase {
    public WebDriver webDriver;
    Logger log;

    TestCase() {
        log = Logger.getGlobal();
    }

    public void setup() {
        webDriver = new ChromeDriver(); // chromium, actually
        // driver.manage().window().maximize();
    }

    public void teardown() {
        webDriver.quit();
    }

    public void report (final boolean result, final String ifSuccess, final String ifFailed) {
        // todo: пока пусть в общий лог
        log.info (result ? ifSuccess : ifFailed);
    }

    public void report (String state) {
        log.info (state);
    }

    private static Scanner getScanner (final String src) throws IOException {
        Scanner sc;
        if (src.equals ("-"))
            sc = new Scanner (System.in);
        else
            sc = new Scanner (FileSystems.getDefault().getPath (src)); // файл
        return sc;
    }

    public static <T> List<TestData> readTestCases (String src, T ttype)
        throws IOException {
        /**
         * src источник данных (file)
         * constructor(String)
         */
        Scanner sc = getScanner (src);
        ArrayList <TestData> tdl = new ArrayList <TestData>();
        String[] tds;
        String str;
        for (;sc.hasNext();) {
            str = sc.nextLine();
            if (str == null || str.length() < 1)
                continue;
            str = str.split("#")[0].trim();
            if (str.length() < 1)
                continue;
            // tds = str.split ("[^\\\\]\\s+"); // fixme!
            tds = str.split ("\\s+");
            if (tds.length < 2)
                continue;
            for (int i = 0; i < tds.length; ++i)
                if (tds[i].equals("NOP"))
                    tds[i] = "";
                else
                    tds[i] = tds[i].replaceAll ("\\\\\\s", " ");
            try {
                TestData <T> t = new TestData <T> (tds, ttype);
                tdl.add (t);
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return tdl;
    }

}
