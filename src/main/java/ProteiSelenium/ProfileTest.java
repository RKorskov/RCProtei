// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-09 23:00:56 roukoru>

// https://www.swtestacademy.com/page-object-model-java/

package ProteiSelenium;

import java.util.List;

// import ProteiSelenium.Profile;

/**
 * <div id="authPage">
 * Поля формы login'а <label for="%s"> <input id="%s">:
 * loginEmail
 * loginPassword
 * Кнопка <button id="authButton">
 */

public class ProfileTest extends TestCase { // extends TestSuite

    public static void main (final String[] args) throws Exception {
        List <TestData> tdl = readTestCases (args[1], new Profile());
        TestData[] profs = tdl.toArray (new TestData[0]);
        int i = 1;
        for (TestData profdata : profs) {
            new ProfileTest().batchTest (i++, profdata);
        }
    }

    /*
    @Test(priority=1)
    public void testTable() {
        setup();
        ProfilePage profilePage = new ProfilePage (webDriver);
        profilePage.login ("test@protei.ru", "test");
        Profile p1 = new Profile ("x@y.z", "x y z", ProfileSex.MALE, true, false, ProfileVar2i.VAR22);
        profilePage.addProfile (p1);
        List <Profile> profs = profilePage.getProfiles ("dataTable");
        for (Profile p : profs) {
            report (p1.equals(p), "Profile add passed", "Profile add failed");
            System.out.println (p);
        }
        //teardown();
    }
    */

    @Test(priority=1)
    public void batchTest (int n, TestData profile) {
        setup();
        ProfilePage profilePage = new ProfilePage (webDriver);
        profilePage.login ("test@protei.ru", "test"); // should always success
        Profile p1 = (Profile) profile.data;
        profilePage.addProfile (p1);
        List <Profile> profs = profilePage.getProfiles ("dataTable");
        boolean notify;
        if (profile.shouldBe == TestResult.PASS)
            notify = profilePage.popupAlertOK();
        else
            notify = profilePage.popupAlertFail();
        if (!notify) { // no alert -- no success
            report (String.format ("Test %d is failed", n));
            return;
        }
        boolean passed = false;
        for (Profile p : profs) {
            if (p1.equals(p)) {
                passed = true;
                break;
            }
        }
        report (String.format ("Test %d is %s", n,
                               passed == (profile.shouldBe == TestResult.PASS)
                               ? "passed" : "failed"));
        teardown();
    }
}
