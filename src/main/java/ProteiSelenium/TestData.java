// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-10 09:05:32 roukoru>

package ProteiSelenium;

import java.util.Arrays;
import java.lang.reflect.Constructor;
import java.lang.Class;
import java.lang.reflect.Type;
import java.lang.IllegalArgumentException;

// import ProteiSelenium.Login;

enum TestResult {PASS, FAIL;}

public class TestData <T> {
    final TestResult shouldBe;
    T data; // Login or Profile object
    TestData (String[] str, T o) throws IllegalArgumentException {
        shouldBe = str[0].equals("pass") ? TestResult.PASS : TestResult.FAIL;
        String[] sa = Arrays.copyOfRange (str, 1, str.length);

        /*
        Class <T> cls = (Class <T>) o.getClass();
        Constructor <T> con = (Constructor <T>)cls.getDeclaredConstructor (str.getClass());
        data = con.newInstance (sa);
        */

        switch (o.getClass().getSimpleName()) {
        case "Login":
            data = (T) new Login (sa);
            break;
        case "Profile":
            data = (T) new Profile (sa);
            break;
        default:
            throw new IllegalArgumentException ("wrong class");
        }
    }
}
