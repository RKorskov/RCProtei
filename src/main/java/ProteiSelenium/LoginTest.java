// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-09 22:02:09 roukoru>

// https://www.swtestacademy.com/page-object-model-java/

package ProteiSelenium;

import java.util.List;

/**
 * <div id="authPage">
 * Поля формы login'а <label for="%s"> <input id="%s">:
 * loginEmail
 * loginPassword
 * Кнопка <button id="authButton">
 */

public class LoginTest extends TestCase { // extends TestSuite

    public static void main (final String[] args) throws Exception {
        //new LoginTest().correctAll();
        List <TestData> tdl = readTestCases (args[0], new Login());
        TestData[] logs = tdl.toArray (new TestData[0]);
        int i = 1;
        for (TestData logdata : logs) {
            new LoginTest().batchTest (i++, logdata);
        }
    }

    /*
    @Test(priority=1)
    public void correctAll() {
        setup(); // TestCase
        AuthPage authPage = new AuthPage (webDriver);
        authPage.login();
        report (authPage.verifyLogin(), "login success", "login fails");
        teardown(); // TestCase
    }
    */

    @Test(priority=1)
    public void batchTest (int i, TestData creds) {
        if (creds == null) return; // report?
        setup();
        AuthPage authPage = new AuthPage (webDriver);
        authPage.login ((Login)creds.data);
        // report (authPage.verifyLogin() == creds.shouldBe, "passed", "fails");
        String res;
        if (creds.shouldBe == TestResult.PASS) {
            res = String.format ("Test %d is %s", i,
                                 authPage.verifyLogin() == creds.shouldBe ?
                                 "passed" : "fails");
        }
        else {
            res = String.format ("Test %d is %s", i,
                                 authPage.loginFails() ?
                                 "passed" : "fails");
        }
        report (res);
        teardown();
    }

}
