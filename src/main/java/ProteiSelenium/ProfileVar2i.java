// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-09 16:40:00 roukoru>

package ProteiSelenium;

enum ProfileVar2i {
    unset(0),
    VAR21(1),
    VAR22(2),
    VAR23(3);

    final int val;

    ProfileVar2i() {
        this(1);
    }

    ProfileVar2i (final int val) {
        this.val = val;
    }

    public static ProfileVar2i fromString (final String str) {
        switch (str) {
        case "2.1":
            return ProfileVar2i.VAR21;
        case "2.2":
            return ProfileVar2i.VAR22;
        case "2.3":
            return ProfileVar2i.VAR23;
        default:
            return ProfileVar2i.unset;
        }
    }

    public String toString() {
        if (val <= 0)
            return "";
        return String.format ("2.%d", val);
    }

    public int valueOf() {
        return val;
    }
}
