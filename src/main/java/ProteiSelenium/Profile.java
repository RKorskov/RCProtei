// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-09 19:02:51 roukoru>

package ProteiSelenium;

import ProteiSelenium.ProfileSex;
import ProteiSelenium.ProfileVar2i;

public class Profile {
    /**
     * profile variables, element names and so on
     */

    final String name,
        email;
    final ProfileSex sex;
    final boolean var11, var12;
    final ProfileVar2i i2rav;

    // element by id :
    public final static String idEmail = "dataEmail",
        idName = "dataName",
        idSex = "dataGender",
        idVar11 = "dataCheck11",
        idVar12 = "dataCheck12",
        var2iName = "radioGroup",
        idVar21 = "dataSelect21",
        idVar22 = "dataSelect22",
        idVar23 = "dataSelect23",
        idSubmit = "dataSend",
        idTable = "dataTable";

    Profile() {
        this.name = null;
        this.email = null;
        this.sex = ProfileSex.MALE;
        this.var11 = false;
        this.var12 = false;
        this.i2rav = ProfileVar2i.VAR21;
    }

    Profile (String cred) {
        this (cred.split ("\\s+"));
    }

    Profile (String[] cred) {
        this.email = cred[0];
        this.name = cred[1];
        this.sex = ProfileSex.fromString (cred[2]);
        this.var11 = Boolean.parseBoolean (cred[3]);
        this.var12 = Boolean.parseBoolean (cred[4]);
        this.i2rav = ProfileVar2i.fromString (cred[5]);
    }

    Profile (String email, String name, ProfileSex sex,
             boolean var11, boolean var12, ProfileVar2i i2rav) {
        this.name = name;
        this.email = email;
        this.sex = sex;
        this.var11 = var11;
        this.var12 = var12;
        this.i2rav = i2rav;
    }

    public boolean equals (Object o) {
        if (!(o instanceof Profile))
            return false;
        Profile prof = (Profile) o;
        if (name.compareTo (prof.name) == 0 &&
            email.compareTo (prof.email) == 0 &&
            sex == prof.sex &&
            var11 == prof.var11 &&
            var12 == prof.var12 &&
            i2rav == prof.i2rav) return true;
        return false;
    }

    public static String idVar2i (ProfileVar2i prof) {
        switch (prof) {
        case VAR21:
            return idVar21;
        case VAR22:
            return idVar22;
        case VAR23:
            return idVar23;
        default:
            return null;
        }
    }

    public String toString() {
        String var1;
        if (var11 == var12)
            var1 = var11 ? "1.1, 1.2" : "";
        else
            var1 = var11 ? "1.1" : "1.2";
        return String.format ("%s %s %s %s %s", email, name, sex, var1, i2rav);
    }
}
