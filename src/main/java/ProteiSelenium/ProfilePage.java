// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-10 08:57:29 roukoru>

package ProteiSelenium;

import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import ProteiSelenium.Profile;
import ProteiSelenium.ProfileSex;
 
public class ProfilePage extends BasePage {

    Logger log;

    public ProfilePage (WebDriver driver) {
        super (driver);
        log = Logger.getGlobal();
    }
 
    // String baseURL = "file://protei.html";
    String baseURL = "http://192.168.1.8/protei.html";
    // By authButton = By.className("uk-button uk-button-primary uk-width-1-1");
    By authButton = By.id ("authButton");
    By authEmail = By.id ("loginEmail");
    By authPasswd = By.id ("loginPassword");

    public boolean login() {
        return login ("test@protei.ru", "test");
    }

    public boolean login (final String email, final String passwd) {
        webDriver.get (baseURL);
        writeText (authEmail, email);
        writeText (authPasswd, passwd);
        click (authButton);
        By dataSend = By.id ("dataSend");
        waitVisibility (dataSend);
        if (webDriver.findElement (dataSend) != null)
            return true;
        return false;
    }

    private void fillForm (final Profile profile) {
        writeTextById (Profile.idEmail, profile.email);
        writeTextById (Profile.idName, profile.name);
        writeTextById (Profile.idSex, profile.sex);
        writeTextById (Profile.idVar11, profile.var11);
        writeTextById (Profile.idVar12, profile.var12);
        //writeTextById (Profile.idVar2i(profile.i2rav), profile.i2rav);
        writeTextById (Profile.idVar2i(profile.i2rav));
    }

    public void writeTextById (String sid, String text) {
        // form input text
        if (sid == null)
            return;
        By element = By.id (sid);
        waitVisibility (element);
        webDriver.findElement (element).sendKeys(text);
    }

    public void writeTextById (String sid, boolean val) {
        // form checkbox
        if (sid == null)
            return;
        By element = By.id (sid);
        waitVisibility (element);
        WebElement cbox = webDriver.findElement (element);
        if (cbox.isSelected() == val) return;
        //if (cbox.isSelected() != val) {cbox.click(); return;}
        cbox.click();
    }

    public void writeTextById (String sid, ProfileSex sex) {
        // form select
        if (sid == null)
            return;
        By element = By.id (sid);
        waitVisibility (element);
        Select sbox = new Select (webDriver.findElement (element));
        sbox.selectByIndex (sex.valueOf());
        // also, sbox.selectByVisibleText (val);
    }

    public void writeTextById (String sid) {
        // form radio button
        // https://www.ecanarys.com/Blogs/ArticleID/225/Handling-Radio-buttons-and-Checkboxes-using-Selenium-Webdriver
        if (sid == null)
            return;
        By element = By.id (sid);
        waitVisibility (element);
        WebElement rbox = webDriver.findElement (element);
        rbox.click();
    }

    public boolean popupAlertOK() {
        // html: <button class="uk-button uk-button-primary uk-modal-close">Ok</button>
        // selector: body > div.uk-modal.uk-open > div > div > div.uk-modal-footer.uk-text-right > button
        // /html/body/div[3]/div/div/div[2]/button
        //By aok = By.className ("uk-button uk-button-primary uk-modal-close");
        By aok = By.xpath ("/html/body/div[3]/div/div/div[2]/button");
        try {
            waitVisibility (aok);
            click (aok);
            return true;
        }
        catch (Exception ex) {;}
        return false;
    }

    public boolean popupAlertFail() {
        By aerr = By.xpath ("//*[@id=\"emailFormatError\"]/a");
        By aerr2 = By.xpath ("//*[@id=\"blankNameError\"]");
        try {
            waitVisibility (aerr);
            click (aerr);
            return true;
        }
        catch (Exception ex) {;}
        try {
            waitVisibility (aerr2);
            click (aerr2);
            return true;
        }
        catch (Exception ex) {;}
        return false;
    }

    public void addProfile (Profile profile) {
        writeTextById (Profile.idName,  profile.name);
        writeTextById (Profile.idEmail, profile.email);
        writeTextById (Profile.idSex,   profile.sex);
        writeTextById (Profile.idVar11, profile.var11);
        writeTextById (Profile.idVar12, profile.var12);
        writeTextById (Profile.idVar2i(profile.i2rav));
        By bsend = By.id (Profile.idSubmit);
        waitVisibility (bsend);
        click (bsend);
    }

    public List<Profile> getProfiles (String tableId) { // idTable
        WebElement ptable = webDriver.findElement (By.id (tableId));
        List <WebElement> rows = ptable.findElements (By.tagName ("tr"));
        List <Profile> profs = new ArrayList <Profile> ();
        Profile anon;
        for (WebElement row : rows) {
            anon = row2profile (row);
            if (anon != null)
                profs.add (anon);
        }
        return profs;
    }

    public Profile row2profile (WebElement row) {
        // <tr><td>x@y.z</td><td>x y z</td><td>Женский</td><td>1.1, 1.2</td><td></td></tr>
        if (row == null)
            return null;
        List <WebElement> cells = row.findElements (By.tagName ("td"));
        if (cells.size() < 5) return null;
        boolean v11, v12;
        v11 = cells.get(3).getText().indexOf("1.1") > -1; // contains...
        v12 = cells.get(3).getText().indexOf("1.2") > -1;
        return new Profile (cells.get(0).getText(),
                            cells.get(1).getText(),
                            ProfileSex.fromString (cells.get(2).getText()),
                            v11, v12,
                            ProfileVar2i.fromString (cells.get(4).getText()));
    }
}
