package ProteiSelenium;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention (RetentionPolicy.RUNTIME)
@interface Test {
    int priority() default 0;
}
