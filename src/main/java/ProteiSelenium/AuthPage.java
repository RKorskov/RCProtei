// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-10 08:56:18 roukoru>

package ProteiSelenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AuthPage extends BasePage {
 
    public AuthPage (WebDriver driver) {
        super (driver);
    }
 
    // String baseURL = "file://protei.html";
    String baseURL = "http://192.168.1.8/protei.html";
    // By authButton = By.className("uk-button uk-button-primary uk-width-1-1");
    By authButton = By.id ("authButton");
    By authEmail = By.id ("loginEmail");
    By authPasswd = By.id ("loginPassword");

    public AuthPage login () {
        return login (new Login());
    }

    public AuthPage login (Login auth) {
        return login (auth.email, auth.passwd);
    }

    public AuthPage login (final String email, final String passwd) {
        webDriver.get (baseURL);
        writeText (authEmail, email);
        writeText (authPasswd, passwd);
        click (authButton);
        return this;
    }
 
    public TestResult verifyLogin() {
        /**
         * true, if login successes
         * <button class="uk-button uk-button-primary uk-width-1-1" type="submit" id="dataSend">
         */
        By dataSend = By.id ("dataSend");
        try {
            waitVisibility (dataSend);
            return webDriver.findElement (dataSend) == null ? TestResult.FAIL : TestResult.PASS;
        }
        catch (Exception ex) {
            return TestResult.FAIL;
        }
    }

    public boolean loginFails() {
        /**
         * true, if login fails
         * <div id="invalidEmailPassword" class="uk-alert uk-alert-danger" data-uk-alert=""><a href="" class="uk-alert-close uk-close"></a>Неверный E-Mail или пароль</div>
         * <div id="emailFormatError" class="uk-alert uk-alert-danger" data-uk-alert=""><a href="" class="uk-alert-close uk-close"></a>Неверный формат E-Mail</div>
         */
        By invalidAuth = By.id ("invalidEmailPassword");
        By invalidAuth2 = By.id ("emailFormatError");
        try {
            waitVisibility (invalidAuth);
            return webDriver.findElement (invalidAuth) != null;
        }
        catch (Exception ex) {
            //return TestResult.FAIL;
            //return false;
        }
        try {
            waitVisibility (invalidAuth2);
            return webDriver.findElement (invalidAuth2) != null;
        }
        catch (Exception ex) {
            //return TestResult.FAIL;
            return false;
        }
    }

}

