// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-09 21:59:24 roukoru>

package ProteiSelenium;

public enum ProfileSex {
    MALE(0),
    FEMALE(1);
    private int sex;

    ProfileSex (int sex) {
        this.sex = sex;
    }

    public static ProfileSex fromString (final String sex) {
        switch (sex) {
        case "female":
        case "Женский": {
            return ProfileSex.FEMALE;
        }
        case "male":
        case "Мужской":
        default: {
            return ProfileSex.MALE;
        }
        }
    }

    public int valueOf() {
        return sex;
    }

    public String toString() {
        return sex == 0 ? "Мужской" : "Женский";
    }
}
