// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
// -*- eval: (set-language-environment Russian) -*-  
// --- Time-stamp: <2018-12-10 08:55:04 roukoru>

package ProteiSelenium;

public class Login
{
    final String email,
        passwd;

    Login() {
        email = "test@protei.ru";
        passwd = "test";
    }

    Login (String str) {
        this (str.split ("\\s+"));
    }

    Login (String[] cred) {
        // предположим, что в данных не ощибаются ;)
        this.email = cred[0];
        this.passwd = cred[1];
    }

    Login (String email, String passwd) {
        this.email = email;
        this.passwd = passwd;
    }

    public String toString() {
        return String.format ("%s  %s", email, passwd);
    }
}
