-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-  
-*- eval: (set-language-environment Russian) -*-  
--- Time-stamp: <2018-12-09 22:37:11 roukoru>

# 2DO

В приложенном html-файле имеется две формы: форма авторизации и анкета, 
доступная после авторизации. Требуется, используя инструмент Selenium, 
написать автотесты, которые проверяли бы работу этих форм.

Данные для прохождения формы авторизации:
E-Mail: test@protei.ru  
<https://e.mail.ru/compose/?mailto=mailto%3atest@protei.ru>  
Пароль: test

ToDo
====

1. ProfileTest (и др.) : многострочные таблицы
2. TestCase.java: todo: escape whitespace!

Test Cases
==========

По-хорошему, нужено ещё отдельно проверять alert'ы, но так как я из использую для оценки прохождения теста, то отдельно это можно не делать. Но лучше бы сделать.

Запускать как: java -jar target/Protei-1.0-SNAPSHOT-jar-with-dependencies.jar login.txt profile.txt

p.s. интересный regex для email'а, пропускащий дивные конструкции из пробелов. %)
p.p.s. интересно, есть ли на странице ещё баги, кроме мыльного?

# Login

1. correct email & passwd
2. correct email, wrong passwd
3. wrong email & passwd
4. correct email, empty passwd
5. empty email & passwd

# Malformed email:

1. correct name@domain
2. unexisting badname@domain
3. unexisting name@baddomain
4. only name
5. only @domain
6. only @
7. junkname@domain
8. name@junkdomain
9. junkname@junkdomain
9. junkname
10. @junkdomain

# Анкета

Поля:  
EMail <input text dataEmail>  
Имя <input text dataName>  
Пол <select м(ж) dataGender>  
Вариант 1.1 <checkbox dataCheck11>  
Вариант 1.2 <checkbox dataCheck12>  
Вариант 2.1 <radiobutton dataSelect21>  
Вариант 2.2 <radiobutton dataSelect22>  
Вариант 2.3 <radiobutton dataSelect23>  
Добавить <button dataSend>

Таблица <table dataTable>  
E-Mail; Имя; Пол; Выбор 1; Выбор 2;

TestCases
=========

comments are started with a hash ('#') and lasts to the end of line.  
blank lines are ignored  
NOP denotes missing field (e.g., empty password an so)  
embedded spaces should be escaped with backslash ('\ ') (todo!)

# login.txt

(pass|fail) email passwd

# profile.txt

(pass|fail) email name sex 1.1 1.2 (2.1|2.2|2.3)
